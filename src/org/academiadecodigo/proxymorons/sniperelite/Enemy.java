package org.academiadecodigo.proxymorons.sniperelite;

public abstract class Enemy extends GameObject {
    private int maxHealth = 100;
    private int health = maxHealth;
    private boolean isDead = false;

    public void takeDamage(int amount) {
        health -= amount;
    }
     public int getHealth() {
        return health;
     }

     public int getMaxHealth() {
        return maxHealth;
     }
     public void dead() {
        health = 0;
        isDead = true;
     }

    public boolean isDead() {
        return isDead;
    }

    public abstract void hit(int damage);

    @Override
    public String getMessage() {
        return "A direct hit! Health: " + getHealth() + " / " + getMaxHealth();
    }
}
