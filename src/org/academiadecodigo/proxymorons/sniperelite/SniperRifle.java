package org.academiadecodigo.proxymorons.sniperelite;

public class SniperRifle {
    private int bulletDamage = 40;

    public void shootEnemy(Enemy enemy) {
        while (!enemy.isDead()) {
            enemy.hit(bulletDamage);
        }
    }
}
