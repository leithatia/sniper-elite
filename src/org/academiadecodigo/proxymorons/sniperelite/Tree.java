package org.academiadecodigo.proxymorons.sniperelite;

public class Tree extends GameObject {
    @Override
    public String getMessage() {
        return "It's a tree! Hold your fire!";
    }
}
