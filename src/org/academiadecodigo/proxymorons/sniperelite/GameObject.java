package org.academiadecodigo.proxymorons.sniperelite;

public abstract class GameObject {
    public abstract String getMessage();

    public boolean equals(GameObject obj) {
        return this == obj;
    }
}
