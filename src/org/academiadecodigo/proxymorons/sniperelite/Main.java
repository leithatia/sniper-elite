package org.academiadecodigo.proxymorons.sniperelite;

public class Main {
    public static void main(String[] args) {
        Game game = new Game();
        game.start();
    }
}
