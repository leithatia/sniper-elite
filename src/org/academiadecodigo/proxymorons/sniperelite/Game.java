package org.academiadecodigo.proxymorons.sniperelite;

public class Game {
    private GameObject[] gameObjects;
    private SniperRifle sniperRifle;
    private int shotsFired;
    private final float spawnRatio = 0.3f;

    public void start() {
        gameObjects = createObjects(10);
        sniperRifle = new SniperRifle();
        startShooting();
    }

    public GameObject[] createObjects(int totalNUmOfObjects) {
        GameObject[] gameObjects = new GameObject[totalNUmOfObjects];
        for (int i = 0; i < totalNUmOfObjects; i++) {
            gameObjects[i] = createNewObject();
        }
        return gameObjects;
    }

    private GameObject createNewObject() {
        if (spawnRatio > Math.random()) {
            return new Tree();
        } else {
            return (0.5 < Math.random()) ? new SoldierEnemy() : new ArmouredEnemy();
        }
    }

    private void startShooting() {
        for (GameObject obj : gameObjects) {

            if (obj instanceof Tree) {
                System.out.println(obj.getMessage());
                continue;
            }

            System.out.println("Target acquired!");
            sniperRifle.shootEnemy((Enemy) obj);
        }
    }
}
