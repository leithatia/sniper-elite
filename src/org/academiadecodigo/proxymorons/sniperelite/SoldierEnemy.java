package org.academiadecodigo.proxymorons.sniperelite;

public class SoldierEnemy extends Enemy {

    @Override
    public void hit(int damage) {
        if (damage >= getHealth()) {
            dead();
            System.out.println("Enemy down!");
            return;
        }

        takeDamage(damage);
        System.out.println("Direct hit! Took " + damage + " damage. Health: " + getHealth() + "/" + getMaxHealth());
    }
}
