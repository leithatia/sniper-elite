package org.academiadecodigo.proxymorons.sniperelite;

public class ArmouredEnemy extends Enemy {
    private int armour = 50;

    @Override
    public void hit(int damage) {
        if (armour > 0) {
            damageArmour(damage);
            return;
        }

        if (damage >= getHealth()) {
            dead();
            System.out.println("Enemy down!");
            return;
        }

        takeDamage(damage);
        System.out.println("Direct hit! Took " + damage + " damage. Health: " + getHealth() + "/" + getMaxHealth());
    }


    private void damageArmour(int damage) {
        if (damage >= armour) {
            armour = 0;
        } else {
            armour -= damage;
        }

        System.out.println("Armour hit! Armour left: " + armour);
    }
}
